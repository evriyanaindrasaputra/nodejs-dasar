const {  saveData } = require('./todo-app')
const readline = require('node:readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
const util = require('node:util');
const question = util.promisify(rl.question).bind(rl);

const main = async () => {
  try {
    const todo = await question('Todo Here ');
    if(todo.trim().length > 0) {
      saveData(todo)    
    }else{
      throw new Error(`empty`);
    }
  } catch (err) {
    console.error('Question rejected', err);
  } finally {
    rl.close()  
  }
}

main()
